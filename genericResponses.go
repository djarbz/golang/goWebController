package goWebController

import (
	"fmt"
	"net/http"
)

// ServerError returns a HTTP 500 Server Error
func (b *Base) ServerError(w http.ResponseWriter) {
	status := http.StatusInternalServerError
	http.Error(w, fmt.Sprintf("%d: Server Error", status), status)
}

// NotImplemented returns a HTTP 501 Not Implemented Error
//noinspection GoUnnecessarilyExportedIdentifiers
func (b *Base) NotImplemented(w http.ResponseWriter) {
	status := http.StatusNotImplemented
	http.Error(w, fmt.Sprintf("%d: Page not implemented yet", status), status)
}
