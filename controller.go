package goWebController

import (
	"fmt"
	"net/http"
	"strings"
)

type routeHandler interface {
	Handler(method string, path string, handler http.Handler)
}

type controllerLogger interface {
	PrintF(string, ...interface{})
}

// Base is a controller template
type Base struct {
	Path             string
	Methods          methods
	SubRoutes        []*Base
	middleware       []Middleware
	parentMiddleware []Middleware
}

// ListRoutes will compile a list of all routes matching the given method(s).
// Pass no arguments to get all methods.
//
// Valid methods are as follows:
// http.MethodGet
// http.MethodHead
// http.MethodPost
// http.MethodPut
// http.MethodDelete
// http.MethodConnect
// http.MethodOptions
// http.MethodTrace
// http.MethodPatch
func (b *Base) ListRoutes(methods ...string) []string {
	if methods == nil {
		methods = append(methods, http.MethodGet, http.MethodHead, http.MethodPost, http.MethodPut, http.MethodDelete,
			http.MethodConnect, http.MethodOptions, http.MethodTrace, http.MethodPatch)
	}

	var routes []string
	for _, method := range methods {
		switch method {
		case http.MethodGet:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodHead:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodPost:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodPut:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodDelete:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodConnect:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodOptions:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodTrace:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		case http.MethodPatch:
			routes = append(routes, fmt.Sprintf("%s: %s", method, b.Path))
		}
	}

	if b.SubRoutes != nil {
		for _, subRoute := range b.SubRoutes {
			routes = append(routes, subRoute.ListRoutes(methods...)...)
		}
	}

	return routes
}

// Register the routes for this controller and all sub controllers
// logger is optional, can be nil
func (b *Base) Register(router routeHandler, parentPath string, logger controllerLogger) {
	b.register(router, parentPath, logger, nil, nil)
}

// Registers the routes for this controller and all sub controllers with middleware
func (b *Base) register(router routeHandler, parentPath string, logger controllerLogger, parent []Middleware, controller []Middleware) {
	// Register parent middleware
	b.addParentMiddleware(parent...)
	b.addParentMiddleware(controller...)

	// Create URI path based on parent and this controller
	path := strings.TrimSuffix(parentPath, "/") + "/" + strings.TrimPrefix(b.Path, "/")
	if logger != nil {
		logger.PrintF("Registering %s", path)
	}

	// Register this controller's Handlers only if they exist
	handlers := b.Methods
	handlers.GET.register(router, http.MethodGet, path, b.parentMiddleware, b.middleware)
	handlers.HEAD.register(router, http.MethodHead, path, b.parentMiddleware, b.middleware)
	handlers.POST.register(router, http.MethodPost, path, b.parentMiddleware, b.middleware)
	handlers.PUT.register(router, http.MethodPut, path, b.parentMiddleware, b.middleware)
	handlers.DELETE.register(router, http.MethodDelete, path, b.parentMiddleware, b.middleware)
	handlers.CONNECT.register(router, http.MethodConnect, path, b.parentMiddleware, b.middleware)
	handlers.OPTIONS.register(router, http.MethodOptions, path, b.parentMiddleware, b.middleware)
	handlers.TRACE.register(router, http.MethodTrace, path, b.parentMiddleware, b.middleware)
	handlers.PATCH.register(router, http.MethodPatch, path, b.parentMiddleware, b.middleware)

	// Register any subRouters
	for _, route := range b.SubRoutes {
		route.register(router, path, logger, b.parentMiddleware, b.middleware)
	}
}

// AddSubRouter adds a sub router to the controller
func (b *Base) AddSubRouter(controller *Base) {
	b.SubRoutes = append(b.SubRoutes, controller)
}

// AddMiddleware appends a middleware func to the controller
func (b *Base) AddMiddleware(handlerFunc ...Middleware) {
	b.middleware = append(b.middleware, handlerFunc...)
}

// addParentMiddleware appends parent middleware to the controller
func (b *Base) addParentMiddleware(handlerFunc ...Middleware) {
	b.parentMiddleware = append(b.parentMiddleware, handlerFunc...)
}
