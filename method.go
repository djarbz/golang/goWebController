package goWebController

import (
	"net/http"
)

type methods struct {
	GET     method
	POST    method
	DELETE  method
	PUT     method
	HEAD    method
	PATCH   method
	OPTIONS method
	CONNECT method
	TRACE   method
}

type method struct {
	Handler          http.HandlerFunc
	middleware       []Middleware
	parentMiddleware []Middleware
	RegisterFunc     func()
}

// Handle applies any associated middleware and handles the request
func (m *method) handle() (handler http.Handler) {
	handler = m.Handler

	// Add Method middleware to the handler
	if len(m.middleware) > 0 {
		handler = AddMiddleware(handler, m.middleware...)
	}

	// Add Parent middleware to the handler
	if len(m.parentMiddleware) > 0 {
		handler = AddMiddleware(handler, m.parentMiddleware...)
	}

	// Return the handler wrapped in all middleware
	return handler
}

func (m *method) register(router routeHandler, method string, path string, parent []Middleware,
	controller []Middleware) {
	if m.Handler != nil {
		m.addParentMiddleware(parent...)
		m.addParentMiddleware(controller...)
		router.Handler(method, path, m.handle())
	}

	if m.RegisterFunc != nil {
		m.RegisterFunc()
	}
}

// AddMiddleware appends a middleware func to the method
//noinspection GoUnnecessarilyExportedIdentifiers
func (m *method) AddMiddleware(handlerFunc ...Middleware) {
	m.middleware = append(m.middleware, handlerFunc...)
}

// addParentMiddleware appends parent middleware to the method
func (m *method) addParentMiddleware(handlerFunc ...Middleware) {
	m.parentMiddleware = append(m.parentMiddleware, handlerFunc...)
}
